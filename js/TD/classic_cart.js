window.onload = function() {
	toast('Déplacez les articles dans le "panier"', 4000);
	parseAlbum();
	fill();
};

var albums = [];
var article = null;

function Album(id, name, year, cover){
	this.id = id;
	this.name = name;
	this.year = year;
	this.cover = cover;
}

function parseAlbum(){
	var listAlbum = JSON.parse(document.getElementById('data').text);
	for(var i = 0; i<listAlbum.length; ++i){
		albums[i] = new Album(listAlbum[i].Code, listAlbum[i].Titre, listAlbum[i].An, listAlbum[i].URL);
	}
}

function fill() {
	var list = document.getElementById('articles');

	for(var i=0;i<albums.length; i++){
		var img = document.createElement('img');
		img.className = "article";
		img.draggable = false;
		img.id = albums[i].id;
		img.name = albums[i].name;
		img.src = albums[i].cover;
		img.width = 128;
		img.alt = albums[i].year;
		list.appendChild(img);
	}
}

document.onmousemove = function(event)
{
	document.getElementById('mousePos').innerHTML = event.pageX + ", " + event.pageY;
	if (!article)
		return;

	if (article.parentNode.id == "articles") {
		article = article.cloneNode(true);
		document.body.appendChild(article);
	}
	if (article.className == 'article') {
		article.style.position = 'absolute';
		article.style.left = event.pageX - article.width / 2 ;
		article.style.top = event.pageY - article.height / 2 ;
		article.parentNode.appendChild(article);
		document.body.appendChild(article);
	} else{
		article = null;
	};
}

document.onmousedown = function(event)
{
	if (article == null)
		article = document.elementFromPoint(event.clientX, event.clientY);
}


document.onmouseup = function(event)
{
	var cart = document.getElementById("cart");
	var cartRect = cart.getBoundingClientRect();

	var shelf = document.getElementById('articles');
	var shelfRect = shelf.getBoundingClientRect();

	if (article != null) {
		var articleRect = article.getBoundingClientRect();

		if (article.className == "article" && article.parentNode.id != "cart") {
			if ((cartRect.left <= event.clientX) && (event.clientX <= cartRect.right) && (cartRect.top <= event.clientY) && (event.clientY <= cartRect.bottom)) {
				var list = document.getElementById("list");
				var li = document.createElement('li');

				var img = document.createElement('img');
				img.className = "circle responsive-img";
				img.src = article.src;
				img.width = 42;
				img.id = article.id;

				var product = document.createElement('span');
				product.className = "title";
				product.innerHTML = article.name + " - " + article.alt;

				var close = document.createElement('a');
				close.className = "secondary-content";
				close.onclick = deletearticle;
				var closeIcon = document.createElement('i');
				closeIcon.className = "mdi-navigation-close";
				close.appendChild(closeIcon);

				li.className = "collection-item avatar";
				li.appendChild(img);
				li.appendChild(product);
				li.appendChild(close);
				list.appendChild(li);

				
				//cart.appendChild(article);
				//document.body.removeChild(article);
				cart.parentNode.parentNode.parentNode.appendChild(article);
				article = null;


			} else if (article.parentNode == document.body && (shelfRect.left <= articleRect.left) && (articleRect.right <= shelfRect.right) && (shelfRect.top <= articleRect.top) && (articleRect.bottom <= shelfRect.bottom)) {
				document.body.removeChild(article);
				article = null;
			}
		}
	}
}

function deletearticle() {
	var li = this.parentNode;
	li.parentNode.removeChild(li);
}