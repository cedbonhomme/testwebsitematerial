$(document).ready(function () {
	var i = 0;
	var indead = !new Boolean( false );
	$("#button1").bind('click', function () {
		if (!indead) {
			$("h1, h2, h3").each(function(i) {
			var current = $(this);
			current.attr("id", "title" + i);
			$("#toc").append("<li><a id='link" + i + "' href='#title" +
				i + "' title='" + current.attr("tagName") + "'>" + 
				current.html() + "</a></li>");
			indead = !indead;
		});
		} else{
			$('#toc').empty();
			indead = !indead;
		};
	});
});