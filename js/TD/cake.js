angular.module('initExample',[])controller('controller', ['$scope', (function($scope) {
	$scope.list = [
	{name:'Farine', price:0.58, quantity:375, unit:'g'},
	{name:'Sucre', price:1.20, quantity:50, unit:'g'},
	{name:'Oeufs', price:0.15, quantity:4, unit:'u'},
	{name:'Lait', price:0.79, quantity:50, unit:'cl'},
	{name:'Beurre', price:7.9, quantity:20, unit:'g'}
	];
} ) ] );