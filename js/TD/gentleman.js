jQuery(document).ready(function($) {

	var el = document.getElementById('items');
	var sortable = Sortable.create(el);

	weather("Bordeaux");
	flickr("mount rainier");
	time();

	$( "#youtube_form" ).submit(function( event ) {
		$("#ytplayer").attr("src","http://www.youtube.com/embed?listType=search&list="+$( "#search_yt" ).val());
		$('#search_yt').val('').blur();
		event.preventDefault();
	});

	$( "#weather_form" ).submit(function( event ) {
		weather($( "#search_weather" ).val());
		$('#search_weather').val('').blur();
		event.preventDefault();
	});

	$( "#flickr_form" ).submit(function( event ) {
		flickr($( "#search_flickr" ).val());
		$('#search_flickr').val('').blur();
		event.preventDefault();
	});
});

var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
var weatherAPI = "http://api.openweathermap.org/data/2.5/weather?callback=?"

function weather(query) {
	$.getJSON( weatherAPI, {q: query,units:'metric'}, function(json, textStatus) {
		$('#weather_title').empty().append(json.name);
		$('#weather_day_info').empty();

		var newdiv = $('<div id="weather_day_info_row" class="row"></div>');
		$('#weather_day_info').append(newdiv);
		$('#weather_day_info').append('<blockquote>'+json.weather[0].description+'</blockquote>');


		$('#weather_day_info_row').append('<div class="col s2 offset-s4"><img class="activator" src="http://openweathermap.org/img/w/'+json.weather[0].icon+'.png"></div>');
		$('#weather_day_info_row').append('<div class="col s6"><h4>'+ parseInt(json.main.temp) +'℃</h4>'+'</div>');	

		$('.preloader').fadeOut('slow/400/fast');
	});
}

function flickr(query) {
	$.getJSON( flickerAPI, {
		tags: query,
		tagmode: "any",
		format: "json"
	}).done(function( data ) {
		$('#flickr_list').empty();
		$.each( data.items, function( i, item ) {
			$('#flickr_list').append(
				$('<li></li>').append(
					$('<img>').attr('src', item.media.m)
					)
				);
		});
		$('.slider').slider({full_width: true});
	});
}

function addZero(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

function time() {
	var d = new Date();
	var x = document.getElementById("hour");
	var h = addZero(d.getHours());
	var m = addZero(d.getMinutes());
	var s = addZero(d.getSeconds());
	x.innerHTML = h + ":" + m + ":" + s;
	setTimeout("time()", 1000)
}

