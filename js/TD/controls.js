function switchToInput(id){
	var origin = document.getElementById(id);
	var parent = origin.parentNode;
	var input = document.createElement("input");
	input.type = "text";
	input.id = id;
	parent.replaceChild(input,origin);
	input.value = origin.firstChild.nodeValue;
}

function switchToSpan(){
	var name = document.getElementById("name");
	var x = document.createElement("SPAN");
	var y = document.createTextNode(name.value);
	x.appendChild(y);
	name.parentNode.replaceChild(x,name);

	var firstname = document.getElementById("firstname");
	var b = document.createElement("SPAN");
	var r = document.createTextNode(firstname.value);
	b.appendChild(r);
	firstname.parentNode.replaceChild(b,firstname);
} 

function newCell(){
	var value = prompt("Contenu de la cellule à insérer: ", 42);
	if (value != null) {
		var table = document.getElementById("table1");

		var cell = document.createElement("td");
		cell.appendChild(document.createTextNode(value));


		table.appendChild(cell);
	};

}