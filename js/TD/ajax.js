var xmlhttp = new XMLHttpRequest();
var musique = [];

function connect(page) {
	if (xmlhttp != null) {
		xmlhttp.open("GET", page, true);
		xmlhttp.onreadystatechange = handler;
		xmlhttp.send();
	} else {
		alert("Pas de support AJAX (XMLHTTP");
	}
}

function handler() {
	if (xmlhttp.readyState == 4) {
		if (xmlhttp.status == 200) {
			display();
		};
	};
}

function call() {
	//var page = "musician.php";
	var page = "musician.php?initial=" + document.getElementById("search").value;
	connect(page);
}

function display() {
	var docJSON = JSON.parse(xmlhttp.responseText);
	for (var i = 0; i < docJSON.length; i++) {
		musique[i] = docJSON[i].Nom_Musicien;
	}

	var display = document.getElementById('jsonSrc');
	display.innerHTML = xmlhttp.responseText;

	clean();
	fill();
}

function clean() {
	var list = document.getElementById('list');
	while (list.hasChildNodes()) {   
		list.removeChild(list.firstChild);
	}

	var tab = document.getElementById('tb').getElementsByTagName('tbody')[0];
	while (tab.hasChildNodes()) {   
		tab.removeChild(tab.firstChild);
	}
}
function fill() {

	var list = document.getElementById('list');
	for(var i = 0; i<musique.length; i++){
		var li = document.createElement('li');
		li.className = "collection-item";
		var namefirstname = document.createTextNode(musique[i]);
		li.appendChild(namefirstname);
		list.appendChild(li);
	}

	var tab = document.getElementById('tb').getElementsByTagName('tbody')[0];
	for(var i=0;i<musique.length; i++){
		var newRow = tab.insertRow(tab.rows.lengh);

		var cellName = newRow.insertCell(0);

		var name = document.createTextNode(musique[i]);

		cellName.appendChild(name);
	}
}