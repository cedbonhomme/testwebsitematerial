var musique = [];
function Musicien(name, firstname){
	this.name = name;
	this.firstname = firstname;
}

function display() {
	var display = document.getElementById('jsonSrc');
	display.innerHTML = document.getElementById('data').text;

	parseMusician();
	fill();
}

function parseMusician(){

	var musiciens = JSON.parse(document.getElementById('data').text);
	for(var i = 0; i<musiciens.length; ++i){
		musique[i] = new Musicien(musiciens[i].Nom_Musicien, musiciens[i].Prénom_Musicien);
	}
}

function fill(){

	var list = document.getElementById('list');
    for(var i = 0; i<musique.length; i++){
    	var li = document.createElement('li');
    	li.className = "collection-item";
    	var namefirstname = document.createTextNode(musique[i].name + " " + musique[i].firstname);
    	li.appendChild(namefirstname);
    	list.appendChild(li);
    }

	var tab = document.getElementById('tb').getElementsByTagName('tbody')[0];
    for(var i=0;i<musique.length; i++){
    	var newRow = tab.insertRow(tab.rows.lengh);

    	var cellName = newRow.insertCell(0);
    	var cellFirstname = newRow.insertCell(1);

    	var name = document.createTextNode(musique[i].name);
    	var firstname = document.createTextNode(musique[i].firstname);

    	cellName.appendChild(name);
    	cellFirstname.appendChild(firstname);
    }
}
