window.onload = function() {
  toast('Déplacez les fruits dans le "panier"', 4000);
};

var fruit = null;

document.onmousemove = function(event)
{
	document.getElementById('mousePos').innerHTML = event.pageX + ", " + event.pageY;
	if (!fruit)
		return;

	if (fruit.parentNode.id == "fruits") {
		fruit = fruit.cloneNode(true);
		document.body.appendChild(fruit);
	}
	if (fruit.className == 'fruit') {
		fruit.style.position = 'absolute';
		fruit.style.left = event.pageX - fruit.width / 2 ;
		fruit.style.top = event.pageY - fruit.height / 2 ;
		fruit.parentNode.appendChild(fruit);
		document.body.appendChild(fruit);
	} else{
		fruit = null;
	};

}


document.onmousedown = function(event)
{
	if (fruit != null) {
		document.body.appendChild(fruit);
	} else{
		fruit = document.elementFromPoint(event.clientX, event.clientY);
	};

	var cartRect = document.getElementById("cart").getBoundingClientRect();
	var fruitRect = fruit.getBoundingClientRect();
}


document.onmouseup = function(event)
{
	var cart = document.getElementById("cart");
	var cartRect = cart.getBoundingClientRect();

	if (fruit.className == "fruit" && fruit.parentNode.id != "cart") {

		if ((cartRect.left <= event.clientX) && (event.clientX <= cartRect.right) && (cartRect.top <= event.clientY) && (event.clientY <= cartRect.bottom)) {
			var list = document.getElementById("list");
			var li = document.createElement('li');

			var img = document.createElement('img');
			img.className = "circle responsive-img";
			img.src = fruit.src;
			img.width = 42;

			var product = document.createElement('span');
			product.className = "title";
			product.innerHTML = fruit.name;

			var close = document.createElement('a');
			close.className = "secondary-content";
			close.onclick = deleteFruit;
			var closeIcon = document.createElement('i');
			closeIcon.className = "mdi-navigation-close";
			close.appendChild(closeIcon);

			li.className = "collection-item avatar";
			li.appendChild(img);
			li.appendChild(product);
			li.appendChild(close);
			list.appendChild(li);
		}
	}
	fruit = null;
}

function deleteFruit() {
	var li = this.parentNode;
	li.parentNode.removeChild(li);
}