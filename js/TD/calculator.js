function addNumber(number) {
	var pending = document.getElementById('pending');
	var save = document.getElementById('save');
	var display = document.getElementById('display');
	var operand = document.getElementById('operand');

	if (display == '') {
		pending.value = number;
		display.innerHTML = number;
	} else{
		pending.value += number;
		display.innerHTML += number;
	};
}

function clean() {
	var pending = document.getElementById('pending');
	var save = document.getElementById('save');
	var display = document.getElementById('display');
	pending.value = '';
	save.value = '';
	display.innerHTML = '';
}

function setOperand(symbol) {
	var pending = document.getElementById('pending');
	var save = document.getElementById('save');
	var display = document.getElementById('display');
	var operand = document.getElementById('operand');

	if (operand.value == '') {
		display.innerHTML += symbol;
		operand.value = symbol;
	save.value = pending.value;
	pending.value = '';
	};

}

function compute() {
	var pending = document.getElementById('pending');
	var save = document.getElementById('save');
	var display = document.getElementById('display');
	var operand = document.getElementById('operand');

	var equation = save.value + operand.value + pending.value;

	if (save != '' && pending != '') {
		pending.value = eval(equation);
		display.innerHTML = pending.value;
		operand.value = '';
	};

	save.value = '';	
}